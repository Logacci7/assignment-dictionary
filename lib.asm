global exit, string_length, print_string, print_char, print_newline, print_uint, print_int, string_equals, read_char, read_word, parse_uint, parse_int, string_copy, print_offset

section .text


; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
   xor rax, rax
   .loop:
   	cmp byte [rdi + rax], 0
   	je .end
   	inc rax
   	jmp .loop
    .end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, rsp

    mov rax, rdi
    dec rsp
    mov [rsp], byte 0
    mov rbx, 10

    .make:
	xor rdx, rdx
	div rbx
	dec rsp
	add rdx, '0'
	mov [rsp], dl
	test rax, rax
	jnz .make

    .print:
	mov rdi, rsp
	call print_string
    mov rsp, r8
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi
    cmp rax, 0
    jge .print
    neg rax
    mov rdi, '-'
    push rax
    call print_char
    pop rax
    .print:
	mov rdi, rax
	call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rdx, rdx
    call string_length

    mov rdx, rax	; rdx - 1 str len
    push rdi 
    mov rdi, rsi
    call string_length	; rax - 2 str len

    pop rdi
    cmp rdx, rax
    jne .no

    xor rax, rax
    xor rcx, rcx

    .loop:
	mov al, byte[rdi + rcx]
	cmp al, byte[rsi + rcx]
	jne .no
	cmp rdx, rcx
	je .yes
	inc rcx
	jmp .loop
    .yes:
	mov rax, 1
	jmp .end
    .no:
	xor rax, rax
    .end:
    	ret 

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov rax, 0
    mov rdi, 0
    mov [rsp], al
    mov rsi, rsp
    mov rdx, 1
    syscall
    mov al, [rsp]
    inc rsp
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi  ;Address
    mov r9, rsi  ;Size
    xor r10, r10 ;Counter
    .space:
	test r10, r10
 	jnz .we
    .loop:
	cmp r10, r9
	jae .fail
	call read_char
	cmp al, 0x20
	je .space
	cmp al, 0x9
	je .space
	cmp al, 0xA
	je .space
	test al, al
	jz .we
	mov [r8 + r10], al
	inc r10
	jmp .loop
    .fail:
	xor rax, rax
	jmp .end
    .we:
	mov [r8 + r10], byte 0
	mov rax, r8
	mov rdx, r10
    .end:
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx
    xor rax, rax
    mov rdx, 1
    .loop:
	mov r8b, [rdi]
	inc rdi
	cmp r8b, '0'
	jb .ch
	cmp r8b, '9'
	jg .ch
	mul rdx
	mov rdx, 10
	sub r8b, '0'
	add al, r8b
	inc rcx
	jmp .loop
    .ch:
	test rcx, rcx
	jnz .good
	xor rdx, rdx
	jmp .end
    .good:
	mov rdx, rcx
    .end:
	inc rcx
	sub rdi, rcx
    	ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov r8b, [rdi]
    inc rdi
    cmp r8b, '-'
    jne .no

    call parse_uint
    test rdx, rdx
    jz .end
    neg rax
    inc rdx
    jmp .end

    .no:
	dec rdi
	call parse_uint
    .end:
    	ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    .a:
	xor rcx, rcx
	call string_length
	cmp rax, rdx     
	jg .fall
    .iter:
	cmp rdx, rcx
	je .end
	mov al, byte[rdi + rcx]
	mov byte[rsi + rcx], al
        inc rcx
	jmp .iter
    .fall:
	mov rax, 0
    .end:
	mov rax, rcx
    	ret

print_offset:
    mov rdi, rax
    call string_length
    inc rdi		      ;null-term
    add rdi, rax              ;key label + offset
    call print_string
    call print_newline
    mov rdi, 0                ;exit-code
    ret
