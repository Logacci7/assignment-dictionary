global find_word
extern string_equals

section .text
find_word:
    .loop:
	push rdi                ;mark
    	add rdi, 8              ;key
	call string_equals
	test rax, rax           ;opercode
	pop rdi                 ;mark
	jnz .find
	mov rdx, [rdi]
	mov rdi, rdx
	test rdi, rdi
	jz .exit
	jmp .loop
     .find:
        add rdi, 8
	mov rax, rdi            ;mark
     .exit:
	ret
