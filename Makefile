ASM = nasm
NASMFLAGS = -f elf64
LD = ld

all: main clean

main: lib.o main.o dict.o
	$(LD) -o main lib.o main.o dict.o

main.o: main.asm words.inc colon.inc
	$(ASM) $(NASMFLAGS) -o main.o main.asm

dict.o: dict.asm
	$(ASM) $(NASMFLAGS) -o dict.o dict.asm

lib.o: lib.asm
	$(ASM) $(NASMFLAGS) -o lib.o lib.asm

clean:
	rm -rf main.o dict.o colon.o lib.o
