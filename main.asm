%include "words.inc"

%define BUFFER_SIZE 256
%define stderr 2

global _start
extern find_word, exit, read_word, string_length, print_offset

section .data

input_err:
    db "Input error",10, 0
find_err:
    db "Failed in string searching",10, 0

section .text
_start:

.read:
    push rbp                  ;prologue
    mov rbp, rsp

    sub rsp, BUFFER_SIZE
    mov rdi, rsp
    mov rsi, BUFFER_SIZE
    call read_word
    test rax, rax
    jne .find

    leave                     ;epilogue

    mov rdi, input_err
    call err_catcher
    mov rdi, 1
    jmp .exit

.find:
    mov rsi, rax
    mov rdi, next
    call find_word

    leave                     ;epilogue

    test rax, rax
    jne .print
    mov rdi, find_err
    call err_catcher
    jmp .exit

.print:
    call print_offset         ;print explanation

.exit:
    call exit

err_catcher:
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, stderr
    syscall
    ret
